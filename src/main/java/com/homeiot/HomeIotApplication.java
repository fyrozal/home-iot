package com.homeiot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeIotApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeIotApplication.class, args);
	}
}
