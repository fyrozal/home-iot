$(document).ready(function(){
	var deviceId = $('#device_id').attr("value");
	$.ajax({
        url: "/histories?device_id=" + deviceId
    }).then(function(data) {
    	$.each(data, function(index, e) {
    		$('#change-history').append(
    				'<p>' + e.regDate + " 	   " + e.statusKey + " is changed to " +
    				e.statusValue + '</p>');
    	});
       console.log(data);
    }, function(err) {
    	console.log(err.responseJSON);
    });
	
	$('.status-update-btn').click(function(){
		var id = $(this).attr('value'); 
		var value = $("#select-status-" + id + " option:selected").text();
		
		console.log(id);
		console.log(value);

		var param = {
			id: id,
			value: value
		}

		$.ajax({
	        url: "/status",
	        method: "PUT",
	        dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(param)
	    }).then(function(data) {
	    	location.reload();
	    }, function(err) {
	    	console.log(err.responseJSON);
	    	location.reload();
	    });
	});
	
});