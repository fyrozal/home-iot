package com.homeiot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homeiot.repository.HistoryRepository;
import com.homeiot.vo.History;

@Service
public class HistoryService {

	@Autowired
	HistoryRepository historyRepository;
	
	public void saveHistory(Long statusId, Long deviceId, String statusKey, Long statusValue) {
		History history = new History(statusId, deviceId, statusKey, statusValue);
		
		historyRepository.save(history);
		
	}
	
	public List<History> getHistoryList(Long deviceId){
		return historyRepository.findAllByDeviceIdOrderByIdDesc(deviceId);
				
	}
}
