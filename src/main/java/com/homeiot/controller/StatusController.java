package com.homeiot.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.homeiot.service.StatusService;
import com.homeiot.vo.Result;
import com.homeiot.vo.Status;

@RestController
public class StatusController {

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	StatusService statusService;
	
	@GetMapping("/status")
	public Status getStatus(@RequestParam("id") Long id) {
		Status status = statusService.getStatus(id);
		return status;
	}
	
	@GetMapping("/status-list")
	public List<Status> getStatusList(){
		List<Status> statusList = statusService.getStatusList();
		return statusList;
	}
	
	@PutMapping("/status")
	public Object updateStatus(HttpServletResponse response, @RequestBody Status statusParam) {
		boolean isSuccess = statusService.updateStatus(statusParam);
		
		if(isSuccess) {
			return new Result(200, "Success");
			
		}else {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return new Result(500, "Fail");
		}
	}
	

	
	
	
}
