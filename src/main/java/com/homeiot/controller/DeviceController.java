package com.homeiot.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.homeiot.service.DeviceService;
import com.homeiot.vo.Device;
import com.homeiot.vo.Result;

@RestController
public class DeviceController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	DeviceService deviceService;

	@GetMapping("/devices")
	public Object getDevices() {
		List<Device> deviceList = deviceService.getDeviceList();
		return deviceList;
	}
	
	//exercise page 27
	@GetMapping("/device")
	public Device getDevice(@RequestParam("id") Long id) {
		Device device = deviceService.getDevice(id);
		return device;
	}
	
	//exercise page 29
	@PostMapping("/device")
	public Object saveDevice(HttpServletResponse response, @RequestBody Device deviceParam) {
		Device device = new Device(deviceParam.getType(), deviceParam.getModel(), deviceParam.getNickname());

		boolean isSuccess = deviceService.saveDevice(device);
		if(isSuccess) {
			return new Result(200, "Success");
		}else {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return new Result(500, "Fail");
		}
	}
	
	
	//exercise page 31
	@DeleteMapping("/device")
	public Object deleteDevice(HttpServletResponse response, @RequestParam("id") Long id) {
		boolean isSuccess = deviceService.deleteDevice(id);
		
		log.info("id :: "+id);
		
		if(isSuccess) {
			return new Result(200, "Success");
			
		}else {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return new Result(500, "Fail");
		}
	}
	
	
}
