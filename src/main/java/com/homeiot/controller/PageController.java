package com.homeiot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.homeiot.service.DeviceService;
import com.homeiot.service.StatusService;
import com.homeiot.vo.Device;
import com.homeiot.vo.Status;

@Controller
public class PageController {

	@Autowired
	DeviceService deviceService;
	
	@Autowired
	StatusService statusService;
	
	@RequestMapping("/")
	public String getIndexPage() {
		return "index";
	}
	
	@RequestMapping("/detail/{id}")
	public String getDetailPAge(@PathVariable("id") Long id, Model model) {
		Device device = deviceService.getDevice(id);
		model.addAttribute("device", device);
		
		List<Status> statusList = statusService.getStatusList(id);
		model.addAttribute("statusList",statusList);
		
		return "detail";
	}
	

	
}
