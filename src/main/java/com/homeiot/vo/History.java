package com.homeiot.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "history")

public class History {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="status_id")
	private Long statusId;
	
	@Column(name="device_id")
	private Long deviceId;
	
	@Column(name="status_key")
	private String statusKey;
	
	@Column(name="status_value")
	private Long statusValue;
	
	@Column(name="regDate")
	private Date regDate;

	
	public History(Long statusId, Long deviceId, String statusKey, Long statusValue) {
		super();
		this.statusId = statusId;
		this.deviceId = deviceId;
		this.statusKey = statusKey;
		this.statusValue = statusValue;
		this.regDate = new Date();
	}


	
	
	

}
