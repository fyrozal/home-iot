package com.homeiot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.io.Serializable;
import java.util.List;

import com.homeiot.vo.Device;
@Repository
public interface DeviceRepository extends JpaRepository<Device, Serializable>{

	List<Device> findByOrderByRegDate();
	
	//exercise page 27
	Device findOneById(Long id);
}
