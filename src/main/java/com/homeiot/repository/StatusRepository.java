package com.homeiot.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.homeiot.vo.Status;

@Repository
public interface StatusRepository extends JpaRepository<Status, Serializable> {
	Status findOneById(Long id);
	
	List<Status> findAllByOrderByUpdtDateDesc();
	
	List<Status> findByDeviceIdOrderById(Long deviceId);
	
	Status findByDeviceIdAndKey(Long deviceId, String string);
	

}
