package com.homeiot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homeiot.repository.DeviceRepository;
import com.homeiot.vo.Device;

@Service
public class DeviceService {
	
	@Autowired
	DeviceRepository deviceRepository;
	
	public List<Device> getDeviceList(){
		List<Device> deviceList = deviceRepository.findByOrderByRegDate();
		return deviceList;
	}
	
	//exercise page 27
	public Device getDevice(Long id) {
		Device device = deviceRepository.findOneById(id);
		return device;
	}
	
	//exercise page 29
	public boolean saveDevice(Device device) {
		Device result = deviceRepository.save(device);
		boolean isSuccess = true;
		if(result == null) {
			isSuccess = false;
		}
		return isSuccess;
	}
	
	//exercise page 31
	public boolean deleteDevice(Long id) {
		Device result = deviceRepository.findOneById(id);
		if(result == null)
			return false;
		
		deviceRepository.deleteById(id);
		return true;
	}
	

}
