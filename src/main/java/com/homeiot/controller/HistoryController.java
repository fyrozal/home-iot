package com.homeiot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.homeiot.service.HistoryService;
import com.homeiot.vo.History;

@RestController
public class HistoryController {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	HistoryService historyService;
	
	@GetMapping("/histories")
	public List<History> getHistoryList(@RequestParam("device_id") Long deviceId){
		return historyService.getHistoryList(deviceId);
	}

}
