package com.homeiot.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.homeiot.vo.History;

public interface HistoryRepository extends JpaRepository<History, Serializable>{

	List<History> findAllByDeviceIdOrderByIdDesc(Long deviceId);

}
