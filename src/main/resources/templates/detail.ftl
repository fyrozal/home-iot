<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Donghun's Home IoT System</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
    body {
      padding-top: 54px;
    }

    @media (min-width: 992px) {
      body {
        padding-top: 56px;
      }
    }

    .status_name {
      margin-left: 1px;
    }
    </style>
  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Donghun's Home IoT System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-2"></div>
        <div class="col-lg-8">

          <!-- Title -->
          <h1 class="mt-4">${device.nickname}</h1>

          <hr>

          <!-- Preview Image -->
          <div class="row">
            <div class="col-md-6">
              	<#if device.type == 'TV'>
					<img class="img-fluid rounded" src="/img/tv.png" alt="">
				<#elseif device.type == 'LIGHT'>
					<img class="img-fluid rounded" src="/img/light.png" alt="">
				</#if>
            </div>

            <div class="col-md-6">
              <br/>
              <h5>Model: ${device.model}</h5>
              <h5>Type: ${device.type}</h5>
              <h5>Registration Date: ${device.regDate?date}</h5>
            </div>
          </div>
          <hr>

          <div id="status">
          	<#list statusList as status>
	            <div class="row">
	              <div class="col-md-3"><h3 class="ml-5">${status.key}</h3></div>
	              <div class="col-md-3"><h3 class="ml-5">${status.value}</h3></div>
	              <div class="col-md-3">
	                <div class="form-group">
	                  <select class="form-control" id="select-status-${status.id}">
	                    <#list status.min..status.max as val>
			                <option>${val}</option>
			            </#list>
	                  </select>
	                </div>
	              </div>
	              <div class="col-md-3 pl-5">
	                <button class="btn btn-primary status-update-btn" value="${status.id}">Update</button>
	              </div>
	            </div>
            </#list>
          </div>

          <hr>

          <div class="card my-4">
            <h5 class="card-header">Change History</h5>
            <div class="card-body" id="change-history">
            </div>
          </div>

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>
    
    <input type="hidden" id="device_id" value="${device.id}">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>
	<script src="/js/detail.js"></script>
  </body>

</html>
