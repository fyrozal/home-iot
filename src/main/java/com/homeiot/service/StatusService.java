package com.homeiot.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.homeiot.repository.StatusRepository;
import com.homeiot.vo.Device;
import com.homeiot.vo.Status;

@Service
public class StatusService {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	StatusRepository statusRepository;

	@Autowired
	HistoryService historyService;

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	DeviceService deviceService;

	public Status getStatus(Long id) {
		Status status = statusRepository.findOneById(id);

		return status;
	}

	public List<Status> getStatusList() {
		List<Status> statusList = statusRepository.findAllByOrderByUpdtDateDesc();
		return statusList;
	}

	public List<Status> getStatusList(Long deviceId) {
		List<Status> statusList = statusRepository.findByDeviceIdOrderById(deviceId);
		return statusList;
	}

	private boolean isTurnedOn(Long deviceId) {
		Status result = statusRepository.findByDeviceIdAndKey(deviceId, "power");
		boolean isOn = true;
		if (result == null) {
			isOn = false;
		} else {
			if (result.getValue() == 1)
				isOn = true;
			else
				isOn = false;
		}
		return isOn;
	}

	public boolean updateStatus(Status status) {
		Status original = statusRepository.getOne(status.getId());
		boolean isUpdated = true;
		if (original == null) {
			isUpdated = false;
		}
		if (!original.getKey().equalsIgnoreCase("power") && !isTurnedOn(original.getDeviceId())) {
			isUpdated = false;
		}
		if (status.getValue() != null) {
			original.setValue(status.getValue());
		}

		System.out.println("status deviceId : " + status.getDeviceId() + " value : " + status.getValue());

		System.out.println("original deviceId : " + original.getDeviceId() + " value : " + original.getValue());
		statusRepository.save(original);
		historyService.saveHistory(original.getId(), original.getDeviceId(), original.getKey(), original.getValue());
		
		
		Device device = deviceService.getDevice(original.getDeviceId());
		String url = "http://633f1fc1.ngrok.io/status?name=akbar&type="+device.getType()+"&key="+original.getKey()+"&value="+original.getValue();
		
		log.info("Call rest api to "+url);
		restTemplate.put(url,null);
		
		return isUpdated;

	}
}
