$(document).ready(function(){
	$.ajax({
        url: "/devices"
    }).then(function(data) {
    	$.each(data, function(index, e) {
    		console.log(data);
    		if(e.type === 'TV') {
    			$('#tv-device').append(
    					'<h2>' + e.nickname + '</h2>'+
    			        '<h5>Model: ' + e.model + '</h5>'+
    			        '<h5>Type: ' + e.type + '</h5>'+
    			        '<h5>Registration Date: ' + e.regDate + '</h5>'+
    			        '<br/><br/>'+
    			        '<a class="btn btn-primary" href="/detail/' + e.id + '">View Detail</a>'
    			);
    		} else if(e.type === 'LIGHT') {
    			$('#light-device').append(
    					'<h2>' + e.nickname + '</h2>'+
    			        '<h5>Model: ' + e.model + '</h5>'+
    			        '<h5>Type: ' + e.type + '</h5>'+
    			        '<h5>Registration Date: ' + e.regDate + '</h5>'+
    			        '<br/><br/>'+
    			        '<a class="btn btn-primary" href="/detail/' + e.id + '">View Detail</a>'    					
    			);
    		}
    		$('#posts').append(
    				'<div class="card mb-4"> <div class="card-body"> <h2 class="card-title">' + e.title 
    				+ '</h2> <p class="card-text">' + e.content 
    				+ '</p> <a href="/page/detail/' + e.id 
    				+ '" class="btn btn-primary">Read More &rarr;</a> </div> ' 
    				+ '<div class="card-footer text-muted"> Posted on ' + e.updtDate 
    				+ ' by ' + e.user 
    				+ '</div> </div>');
    	});
       console.log(data);
    }, function(err) {
    	console.log(err.responseJSON);
    });
});